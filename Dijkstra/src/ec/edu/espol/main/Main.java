/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.main;

import ec.edu.espol.tdas.Graph;

/**
 *
 * @author Ginger Jacome
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Graph<Integer> grafo= new Graph<>(false);
        grafo.addVertex(1);
        grafo.addVertex(2);
        grafo.addVertex(3);
        grafo.addVertex(4);
        grafo.addVertex(5);
        grafo.addVertex(6);
        grafo.addVertex(7);
        grafo.addEdge(1, 2, 1);
        grafo.addEdge(1, 4, 4);
        grafo.addEdge(4, 7, 4);
        grafo.addEdge(4, 2, 6);
        grafo.addEdge(4, 5, 3);
        grafo.addEdge(2, 5, 4);
        grafo.addEdge(2, 3, 2);
        grafo.addEdge(5, 3, 5);
        grafo.addEdge(3, 6, 6);
        grafo.addEdge(5, 6, 8);
        grafo.addEdge(5, 7, 7);
        grafo.addEdge(7, 6, 3);
        Graph<Integer> nuevo= grafo.kruskal();
        System.out.println(nuevo);
                
                
    }
    
}
