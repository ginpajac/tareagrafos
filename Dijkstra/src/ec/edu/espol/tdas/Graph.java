/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.tdas;

import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Administrador
 */
public class Graph<E> {

    private List<Vertex<E>> vertices;
    private boolean dirigido;

    public Graph(boolean dirigido) {
        this.dirigido = dirigido;
        vertices = new LinkedList<>();

    }

    public boolean addVertex(E data) {
        if (data == null || this.contains(data)) {
            return false;
        }
        return vertices.add(new Vertex(data));
    }

    public Graph<E> prim() {
        Graph<E> retorna = new Graph(this.dirigido);
        if (!this.dirigido) {
            if (!this.vertices.isEmpty()) {
                Vertex<E> v = this.vertices.get(0);
                PriorityQueue<Edge<E>> cola = new PriorityQueue<>((Edge<E> e1, Edge<E> e2) -> e1.getPeso() - e2.getPeso());
                v.setVisitado(true);
                retorna.addVertex(v.getData());
                for (Edge<E> arc : v.getArcos()) {
                    cola.offer(arc);
                }
                while (!cola.isEmpty()) {
                    Edge<E> arco = cola.poll();
                    if (!arco.getDestino().isVisitado()) {
                        arco.getDestino().setVisitado(true);
                        retorna.addVertex(arco.getDestino().getData());
                        retorna.addEdge(arco.getOrige().getData(), arco.getDestino().getData(), arco.getPeso());
                        for (Edge<E> a : arco.getDestino().getArcos()) {
                            if (!a.getDestino().isVisitado()) {
                                cola.offer(a);
                            }
                        }
                    }
                }
            }
        }
        return retorna;
    }

    private void dijkstra(Vertex<E> inicio) {
        inicio.setDistancia(0);
        PriorityQueue<Vertex<E>> cola = new PriorityQueue<>((Vertex<E> v1, Vertex<E> v2) -> v1.getDistancia() - v2.getDistancia());
        cola.offer(inicio);
        while (!cola.isEmpty()) {
            Vertex<E> v = cola.poll();
            for (Edge<E> e : v.getArcos()) {
                Vertex<E> siguiente = e.getDestino();
                int peso = e.getPeso();
                int distancia = v.getDistancia() + peso;
                if (distancia < siguiente.getDistancia()) {
                    cola.remove(siguiente);
                    siguiente.setDistancia(distancia);
                    siguiente.setReferencia(v);
                    cola.offer(siguiente);
                }
            }
        }
    }

    public List<E> caminoMinimo(E origen, E destino) {
        List<E> lista = new LinkedList<>();
        Vertex<E> vo = searhVertex(origen);
        Vertex<E> vf = searhVertex(destino);
        if (vo == null || vf == null) {
            return lista;
        }
        lista.add(vf.getData());
        dijkstra(vo);
        Vertex<E> father = vf.getReferencia();
        lista.add(father.getData());
        while (!father.equals(vo)) {
            father = father.getReferencia();
            lista.add(father.getData());
        }

        return invertirLista(lista);
    }

    private List<E> invertirLista(List<E> inver) {
        List<E> lista = new LinkedList<>();
        ListIterator i = inver.listIterator(inver.size());
        while (i.hasPrevious()) {
            lista.add((E) i.previous());
        }
        return lista;
    }

    public int distanciaMinima(E origen, E destino) {
        Vertex<E> vo = searhVertex(origen);
        Vertex<E> vf = searhVertex(destino);
        if (vo == null || vf == null) {
            return -1;
        }
        dijkstra(vo);
        return vf.getDistancia();
    }

    private boolean contains(E data) {
        for (Vertex v : this.vertices) {
            if (v.getData().equals(data)) {
                return true;
            }
        }
        return false;
    }

    private Vertex<E> searhVertex(E data) {
        for (Vertex<E> v : vertices) {
            if (v.getData().equals(data)) {
                return v;
            }
        }
        return null;
    }

    public boolean addEdge(E origen, E destino, int peso) {
        Vertex<E> vo = searhVertex(origen);
        Vertex<E> vd = searhVertex(destino);
        if (vo == null || vd == null) {
            return false;
        }
        Edge<E> a = new Edge(peso, vo, vd);
        if (vo.getArcos().contains(a)) {
            return false;
        }
        vo.getArcos().add(a);
        if (!dirigido) {
            Edge<E> b = new Edge(peso, vd, vo);
            vd.getArcos().add(b);
        }
        return true;
    }

    public boolean removeVertex(E data) {
        if (data == null) {
            return false;
        }
        ListIterator<Vertex<E>> i = vertices.listIterator();
        while (i.hasNext()) {
            Vertex<E> vertice = i.next();
            if (vertice.getData().equals(data)) {
                for (Edge<E> e : vertice.getArcos()) {
                    removeEdge(vertice.getData(), e.getDestino().getData());
                    removeEdge(e.getOrige().getData(), vertice.getData());
                }
                i.remove();
            }
        }
        return true;
    }

    public boolean removeEdge(E origen, E destino) {
        if (origen == null || destino == null) {
            return false;
        }
        Vertex<E> vo = searhVertex(origen);
        Vertex<E> vd = searhVertex(destino);
        for (Vertex<E> v : this.vertices) {
            for (Edge<E> e : v.getArcos()) {
                boolean bool1 = e.getDestino().equals(vd) && e.getOrige().equals(vo);
                boolean bool2 = e.getDestino().equals(vo) && e.getOrige().equals(vd);
                if (bool1 || bool2) {
                    v.getArcos().remove(e);
                }
            }
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        StringBuilder a = new StringBuilder();
        s.append("Vertices: {");
        a.append("Arcos: {");
        List<List<E>> verifica = new LinkedList<>();
        for (Vertex<E> v : vertices) {
            s.append(v.getData()).append(" ");
            for (Edge<E> e : v.getArcos()) {
                List<E> in = new LinkedList<>();
                List<E> inv = new LinkedList<>();
                in.add(e.getOrige().getData());
                in.add(e.getDestino().getData());
                inv.add(e.getDestino().getData());
                inv.add(e.getOrige().getData());
                if (!verifica.contains(in) && !verifica.contains(inv)) {
                    verifica.add(in);
                }
            }
        }
        for (List<E> lis : verifica) {
            a.append("(").append(lis.get(0)).append(",").append(lis.get(1)).append(")");
        }
        s.append("}");
        a.append("}");
        return s.toString() + "\n" + a.toString();
    }

    public int inDegree(E data) {
        if (data == null) {
            return 0;
        }
        int contador = 0;
        for (Vertex<E> v : vertices) {
            for (Edge<E> e : v.getArcos()) {
                if (e.getDestino().getData().equals(data)) {
                    contador++;
                }
            }
        }
        return contador;
    }

    public int outDegree(E data) {
        ListIterator<Vertex<E>> i = vertices.listIterator();
        while (i.hasNext()) {
            Vertex<E> vertice = i.next();
            if (vertice.getData().equals(data)) {
                return vertice.getArcos().size();
            }
        }
        return 0;
    }

    public boolean isEmpty() {
        return vertices.isEmpty();
    }

    public List<E> bfs(E data) {
        List<E> lista = new LinkedList<>();
        Vertex<E> v = searhVertex(data);
        if (v == null || this.isEmpty()) {
            return lista;
        }
        Queue<Vertex<E>> cola = new LinkedList<>();
        v.setVisitado(true);
        cola.offer(v);
        while (!cola.isEmpty()) {
            Vertex<E> vi = cola.poll();
            lista.add(vi.getData());
            for (Edge<E> e : vi.getArcos()) {
                if (!e.getDestino().isVisitado()) {
                    e.getDestino().setVisitado(true);
                    cola.offer(e.getDestino());
                }
            }
        }
        limpiar();
        return lista;
    }

    public List<E> dfs(E data) {
        List<E> lista = new LinkedList<>();
        Vertex<E> v = searhVertex(data);
        if (v == null || this.isEmpty()) {
            return lista;
        }
        Deque<Vertex<E>> pila = new LinkedList<>();
        v.setVisitado(true);
        pila.push(v);
        while (!pila.isEmpty()) {
            Vertex<E> vi = pila.pop();
            lista.add(vi.getData());
            for (Edge<E> e : vi.getArcos()) {
                if (!e.getDestino().isVisitado()) {
                    e.getDestino().setVisitado(true);
                    pila.push(e.getDestino());
                }
            }
        }
        return lista;
    }

    public Graph<E> invertir() {
        Graph<E> invertido = new Graph<>(this.dirigido);
        for (Vertex<E> v : this.vertices) {
            invertido.addVertex(v.getData());
            for (Edge<E> e : v.getArcos()) {
                invertido.addVertex(e.getDestino().getData());
                invertido.addEdge(e.getDestino().getData(), v.getData(), e.getPeso());
            }
        }
        return invertido;
    }

    public void limpiar() {
        for (Vertex<E> v : this.vertices) {
            v.setVisitado(false);
        }
    }

    public boolean esConexo() {
        if (isEmpty() || this.vertices.size() == 1) {
            return true;
        }
        if (!dirigido) {
            limpiar();
            return bfs(this.vertices.get(0).getData()).size() == this.vertices.size();
        } else {
            Graph<E> invertido = this.invertir();
            return invertido.bfs(invertido.vertices.get(0).getData()).size() == bfs(vertices.get(0).getData()).size();
        }
    }

    public List<List<E>> componenteConexa() {
        List<List<E>> componentes = new LinkedList<>();
        if (!this.dirigido) {
            for (Vertex<E> v : this.vertices) {
                if (!v.isVisitado()) {
                    v.setVisitado(true);
                    List<E> a = this.bfs(v.getData());
                    componentes.add(a);
                }
            }
            this.limpiar();
        } else {
            Graph<E> invertido = this.invertir();
            for (Vertex<E> v : this.vertices) {
                List<E> comp = new LinkedList<>();
                Set<E> a = new HashSet<>();
                Set<E> d = new HashSet<>();
                if (!v.isVisitado()) {
                    a.addAll(this.bfs(v.getData()));
                    d.addAll(invertido.bfs(v.getData()));
                    a.retainAll(d);
                    comp.addAll(a);
                    componentes.add(comp);
                    visitados(comp);
                    desmarca(componentes);
                }
            }
            this.limpiar();
        }
        return componentes;
    }

    private void visitados(List<E> lista) {
        for (Vertex<E> v : this.vertices) {
            for (E e : lista) {
                if (v.getData().equals(e)) {
                    v.setVisitado(true);
                }
            }
        }
    }

    private void desmarca(List<List<E>> lista) {
        Set b = new HashSet<>();
        for (List<E> g : lista) {
            for (E e : g) {
                b.add(e);
            }
        }
        for (Vertex<E> ve : this.vertices) {
            if (!b.contains(ve.getData())) {
                ve.setVisitado(false);
            }
        }
    }

    public boolean existeCiclo() {
        limpiar();
        for (Vertex<E> v : this.vertices) {
            Deque<Vertex<E>> pila = new LinkedList<>();
            v.setVisitado(true);
            pila.push(v);
            while (!pila.isEmpty()) {
                Vertex<E> vi = pila.pop();
                for (Edge<E> e : vi.getArcos()) {
                    if (!e.getDestino().isVisitado()) {
                        pila.push(e.getDestino());
                    } else {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private List<E> getListaComponente(E var) {
        List<E> retorno = new ArrayList<>();
        List<List<E>> lista = this.componenteConexa();
        for (List<E> c : lista) {
            if (c.contains(var)) {
                retorno = c;
                return retorno;
            }
        }
        return retorno;
    }

    private boolean sonComponentesIguales(List<E> c1, List<E> c2) {
        for (int i = 0; i < c1.size(); i++) {
            if (c1.get(i) != c2.get(i)) {
                return false;
            }
        }
        return true;
    }

    public Graph<E> kruskal() {
        int cnt = 0;
        PriorityQueue<Edge<E>> cola = new PriorityQueue<>((Edge<E> e1, Edge<E> e2) -> e1.getPeso() - e2.getPeso());
        Graph<E> grafoKrusal = new Graph<>(false);
        for (Vertex<E> v : this.vertices) {
            grafoKrusal.addVertex(v.getData());
            for (Edge<E> e : v.getArcos()) {
                cola.offer(e);
            }
        }
        while (!cola.isEmpty() && cnt < grafoKrusal.vertices.size() - 1) {
            Edge<E> arco = cola.poll();
            List<E> origenComp = grafoKrusal.getListaComponente(arco.getOrige().getData());
            List<E> destComp = grafoKrusal.getListaComponente(arco.getDestino().getData());
            if (!sonComponentesIguales(origenComp, destComp)) {
                grafoKrusal.addEdge(arco.getOrige().getData(), arco.getDestino().getData(), arco.getPeso());
                cnt++;
            }
        }
        return grafoKrusal;
    }

}
