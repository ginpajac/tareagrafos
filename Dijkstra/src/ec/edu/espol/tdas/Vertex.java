/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.tdas;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Administrador
 */
public class Vertex<E> {
    private E data;
    Vertex<E> referencia;
    int distancia;
    private List<Edge<E>> arcos;
    private boolean visitado;

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public List<Edge<E>> getArcos() {
        return arcos;
    }

    public void setArcos(List<Edge<E>> arcos) {
        this.arcos = arcos;
    }

    public boolean isVisitado() {
        return visitado;
    }

    public void setVisitado(boolean visitado) {
        this.visitado = visitado;
    }
    
    public Vertex(E data) {
        this.referencia=null;
        this.distancia=Integer.MAX_VALUE;
        this.data= data;
        arcos=new LinkedList<>();
        this.visitado= false;
    }

    public Vertex<E> getReferencia() {
        return referencia;
    }

    public void setReferencia(Vertex<E> referencia) {
        this.referencia = referencia;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Vertex) || obj ==null ) {
            return false;
        }
        Vertex v= (Vertex) obj;
        if(v.getData().equals(this.data)){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.data);
        return hash;
    }

    @Override
    public String toString() {
        return ""+data;
    }

    
    
}
